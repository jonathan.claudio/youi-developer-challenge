﻿using OrderReceipts.Domain.Interfaces;
using OrderReceipts.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderReceipts.Domain.ServiceInterfaces
{
    public interface IReceiptService
    {
        SalesReceipt GenerateReceipt(IEnumerable<int> productIds);
    }
}
