﻿using System.Collections.Generic;
using OrderReceipts.Domain.Models;

namespace OrderReceipts.Domain.ServiceInterfaces
{
    public interface IDataService
    {
        decimal GetSalesTaxPercentage();
        List<Product> GetAllProducts();
        Product GetProductById(int id);
    }
}
