﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderReceipts.Domain.Models;
using OrderReceipts.Domain.ServiceInterfaces;
using Newtonsoft.Json;

namespace OrderReceipts.Domain.Services
{
    public class JsonDataService : IDataService
    {
        private readonly string _productsFile;
        private readonly string _salesTaxFile;

        public JsonDataService(string productsFilename = "Products.json", string salesTaxFilename = "SalesTax.json")
        {
            _productsFile = Path.GetFullPath(productsFilename);
            _salesTaxFile = Path.GetFullPath(salesTaxFilename);
        }

        public decimal GetSalesTaxPercentage()
        {
            return ReadJsonFromFile<SalesTax>(_salesTaxFile).salesTaxPercentage;
        }

        public List<Product> GetAllProducts()
        {
            return ReadJsonFromFile<List<Product>>(_productsFile);
        }

        public Product GetProductById(int id)
        {
            var products = GetAllProducts();
            return products.FirstOrDefault(p => p.Id == id);
        }

        private class SalesTax
        {
            public decimal salesTaxPercentage { get; set; }
        }

        private T ReadJsonFromFile<T>(string filename)
        {
            var json = File.ReadAllText(filename);
            return JsonConvert.DeserializeObject<T>(json);

        }
    }
}
