﻿using OrderReceipts.Domain.Interfaces;
using OrderReceipts.Domain.Models;
using OrderReceipts.Domain.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderReceipts.Domain.Services
{
    public class ReceiptService : IReceiptService
    {
        private readonly IDataService _dataService;
        private readonly decimal _taxRatePercentage;
        public ReceiptService()
        {
            _dataService = new JsonDataService();
            _taxRatePercentage = _dataService.GetSalesTaxPercentage();
        }

        public SalesReceipt GenerateReceipt(IEnumerable<int> productIds)
        {
            var allProducts = _dataService.GetAllProducts();

            var orderProducts = new List<IProduct>();

            foreach (var id in productIds)
            {
                var product = allProducts.FirstOrDefault(p => p.Id == id);

                if (product != null)
                    orderProducts.Add(product);
            }

            var order = new Order(orderProducts);
            return new SalesReceipt(order, _taxRatePercentage);
        }
    }
}
