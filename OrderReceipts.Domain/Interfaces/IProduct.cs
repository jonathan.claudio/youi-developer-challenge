﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderReceipts.Domain.Interfaces
{
    public interface IProduct
    {
        int Id { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        decimal Price { get; set; }
        Dictionary<int, decimal> Discounts { get; set; }
        bool DiscountAsPercentage { get; set; }
    }
}
