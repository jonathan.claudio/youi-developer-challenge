﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderReceipts.Domain.Interfaces
{
    public interface ILineItem : IProduct
    {
        public int Quantity { get; set; }

        public decimal CalculateLineDiscount();

        public decimal CalculateLineTotal();

    }
}
