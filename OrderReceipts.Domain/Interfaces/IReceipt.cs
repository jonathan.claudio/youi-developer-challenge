﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderReceipts.Domain.Interfaces
{
    public interface IReceipt
    {
        Dictionary<int, ILineItem> LineItems { get; }
        DateTime OrderDate { get; }
        decimal CalculateSubtotal();
        decimal CalculateTax();
        decimal CalculateTotalOrderPrice();
        decimal CalculateDiscountTotal();
    }
}
