﻿using OrderReceipts.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderReceipts.Domain.Interfaces
{
    public interface IOrder
    {
        DateTime OrderDate { get; }
        IEnumerable<IProduct> Products { get; }
    }
}
