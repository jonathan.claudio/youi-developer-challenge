﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderReceipts.Domain.Interfaces;

namespace OrderReceipts.Domain.Models
{
    public class Product : IProduct
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Dictionary<int, decimal> Discounts { get; set; }
        public bool DiscountAsPercentage { get; set; }
    }
}
