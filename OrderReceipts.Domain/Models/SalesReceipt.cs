﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderReceipts.Domain.Interfaces;

namespace OrderReceipts.Domain.Models
{
    public class SalesReceipt : IReceipt
    {
        private readonly IOrder _order;
        private readonly decimal _salesTaxRate;
        private readonly Dictionary<int, ILineItem> _lineItems;

        public SalesReceipt(IOrder order, decimal salesTaxPercentage)
        {
            if (salesTaxPercentage < 0)
                throw new ArgumentOutOfRangeException("Sales Tax cannot be zero or negative");

            _order = order;
            _salesTaxRate = salesTaxPercentage / 100;   // Using decimal instead of raw percentage as it's easier to use
            _lineItems = new Dictionary<int, ILineItem>();

            GetProductLineItems();
        }

        public Dictionary<int, ILineItem> LineItems 
        { 
            get
            {
                return _lineItems;
            }
        }

        public DateTime OrderDate
        {
            get
            {
                return _order.OrderDate;
            }
        }

        public decimal CalculateSubtotal()
        {
            if (_order.Products.ToList().Count == 0)
                return 0;

            var totalDiscount = CalculateDiscountTotal();
            return _order.Products.Sum(product => product.Price) - totalDiscount;
        }

        public decimal CalculateTax()
        {
            var subTotal = CalculateSubtotal();
            return subTotal * _salesTaxRate;
        }

        public decimal CalculateTotalOrderPrice()
        {
            var subTotal = CalculateSubtotal();
            var salesTax = CalculateTax();

            return subTotal + salesTax;
        }

        public decimal CalculateDiscountTotal()
        {
            if (_order.Products.ToList().Count == 0)
                return 0;

            var totalDiscount = _lineItems.Sum(lineItem => lineItem.Value.CalculateLineDiscount());

            return totalDiscount;
        }

        private void GetProductLineItems()
        {
            foreach (var product in _order.Products)
            {
                if (_lineItems.ContainsKey(product.Id))
                {
                    _lineItems[product.Id].Quantity++;
                }
                else
                {
                    _lineItems.Add(product.Id, new LineItem(product)
                    {
                        Quantity = 1
                    });
                }
            }
        }
    }
}
