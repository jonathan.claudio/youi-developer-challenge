﻿using OrderReceipts.Domain.Interfaces;

namespace OrderReceipts.Domain.Models
{
    public class LineItem : Product, ILineItem
    {
        public LineItem(IProduct product)
        {
            Id = product.Id;
            Title = product.Title;
            Description = product.Description;
            Price = product.Price;
            DiscountAsPercentage = product.DiscountAsPercentage;
            Discounts = product.Discounts;
        }

        public int Quantity { get; set; }
        public decimal CalculateLineDiscount()
        {
            if (Discounts == null || Discounts.Count == 0)
                return 0;

            var lineItemDiscount = 0m;

            foreach (var bulkQty in Discounts.Keys)
            {
                if (Quantity >= bulkQty)
                {
                    lineItemDiscount = Discounts[bulkQty];
                }
            }

            if (DiscountAsPercentage)
            {
                lineItemDiscount = Price * (lineItemDiscount / 100);
            }

            return lineItemDiscount * Quantity;
        }

        public decimal CalculateLineTotal()
        {
            var lineItemDiscount = CalculateLineDiscount();
            return (Quantity * Price) - lineItemDiscount;
        }
    }
}
