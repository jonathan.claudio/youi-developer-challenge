﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderReceipts.Domain.Interfaces;

namespace OrderReceipts.Domain.Models
{
    public class Order : IOrder
    {
        public Order(IEnumerable<IProduct> products)
        {
            OrderDate = DateTime.UtcNow;
            Products = products;
        }

        public DateTime OrderDate { get; private set; }
        public IEnumerable<IProduct> Products { get; private set; }
    }
}
