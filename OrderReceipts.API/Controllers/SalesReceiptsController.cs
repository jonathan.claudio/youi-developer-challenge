﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderReceipts.Domain.Interfaces;
using OrderReceipts.Domain.ServiceInterfaces;
using System.Collections.Generic;
using System.Text;

namespace OrderReceipts.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesReceiptsController : ControllerBase
    {
        private readonly ILogger<SalesReceiptsController> _logger;
        private readonly IReceiptService _receiptService;

        public SalesReceiptsController(ILogger<SalesReceiptsController> logger, IReceiptService receiptService)
        {
            _logger = logger;
            _receiptService = receiptService;
        }

        [HttpPost]
        [Route("GenerateHtmlReceipt")]
        public IActionResult GenerateHtmlReceipt([FromBody] IEnumerable<int> productIds)
        {
            if (productIds == null)
                return BadRequest();

            var salesReceipt = GenerateSalesReceipt(productIds);
            return Content(GenerateHtmlReceipt(salesReceipt));
        }

        [HttpPost]
        [Route("GenerateTextReceipt")]
        public IActionResult GenerateTextReceipt([FromBody] IEnumerable<int> productIds)
        {
            if (productIds == null)
                return BadRequest();

            var salesReceipt = GenerateSalesReceipt(productIds);
            return Content(GenerateTextReceipt(salesReceipt)); 
        }

        private IReceipt GenerateSalesReceipt(IEnumerable<int> productIds)
        {
            return _receiptService.GenerateReceipt(productIds);
        }

        private string GenerateHtmlReceipt(IReceipt receipt)
        {
            var sb = new StringBuilder();

            sb.AppendLine("<html>");
            sb.AppendLine($"<head><title>Order {receipt.OrderDate.ToString("yyyyMMddhhmmssss")}</title>");
            sb.AppendLine("<style>table, th, td { border-collapse: collapse; border: solid 1px black; padding: 10px; text-align: center }</style>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine($"<h2>Order Date: {receipt.OrderDate.ToLocalTime().ToLongDateString()}</h2><br>");
            
            // Product Tables
            sb.AppendLine("<table>");
            sb.AppendLine("<th>Product</th>");
            sb.AppendLine("<th>Description</th>");
            sb.AppendLine("<th>Price</th>");
            sb.AppendLine("<th>Qty</th>");
            sb.AppendLine("<th>Line Discount</th>");
            sb.AppendLine("<th>Line Total</th>");
            foreach (var item in receipt.LineItems)
            {
                sb.AppendLine("<tr>");
                sb.AppendLine($"<td>{item.Value.Title}</td>");
                sb.AppendLine($"<td>{item.Value.Description}</td>");
                sb.AppendLine($"<td>{item.Value.Price.ToString("C")}</td>");
                sb.AppendLine($"<td>{item.Value.Quantity}</td>");
                sb.AppendLine($"<td>{item.Value.CalculateLineDiscount().ToString("C")}</td>");
                sb.AppendLine($"<td>{item.Value.CalculateLineTotal().ToString("C")}</td>");
                sb.AppendLine("</tr>");
            }
            sb.AppendLine("</table>");

            sb.AppendLine($"<p>Order Subtotal: {receipt.CalculateSubtotal().ToString("C")}</p>");
            sb.AppendLine($"<p>Sales Tax: {receipt.CalculateTax().ToString("C")}</p>");
            sb.AppendLine($"<p>Total Order Price (inc. Tax): {receipt.CalculateTotalOrderPrice().ToString("C")}</p>");
            sb.AppendLine($"<p>Total Discounts: {receipt.CalculateDiscountTotal().ToString("C")}</p>");

            sb.AppendLine("</body></html>");


            return sb.ToString();
        }

        private string GenerateTextReceipt(IReceipt receipt)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Order Date: {receipt.OrderDate.ToLocalTime().ToLongDateString()}");
            sb.AppendLine("Products:");
            sb.AppendLine(" Product | Description | Price | Qty | Line Discount | Line Total ");
            foreach (var item in receipt.LineItems)
            {
                sb.AppendLine(
                    $"{item.Value.Title} | " +
                    $"{item.Value.Description} | " +
                    $"{item.Value.Price.ToString("C")} | " +
                    $"{item.Value.Quantity} |" +
                    $"{item.Value.CalculateLineDiscount().ToString("C")} | " +
                    $"{item.Value.CalculateLineTotal().ToString("C")} | "
                );
            }
            sb.AppendLine($"Order Subtotal: {receipt.CalculateSubtotal().ToString("C")}");
            sb.AppendLine($"Sales Tax: {receipt.CalculateTax().ToString("C")}");
            sb.AppendLine($"Total Order Price (inc. Tax): {receipt.CalculateTotalOrderPrice().ToString("C")}");
            sb.AppendLine($"Total Discounts: {receipt.CalculateDiscountTotal().ToString("C")}");

            return sb.ToString();

        }
    }
}
