﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OrderReceipts.Domain.Models;
using OrderReceipts.Domain.ServiceInterfaces;
using OrderReceipts.Domain.Services;

namespace OrderReceipts.Tests
{
    class SalesReceiptTests
    {
        private IDataService _dataService;
        private IReceiptService _receiptService;
        private decimal _salesTax;

        [SetUp]
        public void SetUp()
        {
            _receiptService = new ReceiptService();
            _dataService = new JsonDataService();
            _salesTax = _dataService.GetSalesTaxPercentage();
        }


        [Test]
        public void TestSalesReceiptSingleItemLineCount()
        {
            var products = new List<int> { 1 };

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(1, receipt.LineItems.Count);

        }

        [Test]
        public void TestSalesReceiptMultipleSameItemsLineCount()
        {
            var productId = 1;

            // Three of the same item
            var products = new List<int> { productId, productId, productId };

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(1, receipt.LineItems.Count);
            Assert.AreEqual(3, receipt.LineItems[productId].Quantity);
        }

        [Test]
        public void TestSalesReceiptMultipleDifferentItemsLineCount()
        {
            var productId1 = 1;
            var productId2 = 2;
            var productId3 = 3;

            var products = new List<int>
            {
                productId1,
                productId1,
                productId1,
                productId2,
                productId2,
                productId3
            };

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(3, receipt.LineItems.Count);
            Assert.AreEqual(3, receipt.LineItems[productId1].Quantity);
            Assert.AreEqual(2, receipt.LineItems[productId2].Quantity);
            Assert.AreEqual(1, receipt.LineItems[productId3].Quantity);
        }

        [Test]
        public void TestSalesReceiptSingleItemSubtotal()
        {
            // Expected value based on product ID 1 with a Qty of 1
            var expectedSubtotal = 5000000m;

            var products = new List<int> { 1 };

            var receipt = _receiptService.GenerateReceipt(products);

            var subtotal = receipt.CalculateSubtotal();

            Assert.AreEqual(expectedSubtotal, subtotal);
        }

        [Test]
        public void TestSalesReceiptSingleItemBulkPercentDiscountSubtotal()
        {
            // Expected value based on product ID 0 with a Qty of 1
            var expectedSubtotal = 350;

            var products = new List<int>();

            for (int i = 0; i < 1000; i++)
            {
                products.Add(0);
            }

            var receipt = _receiptService.GenerateReceipt(products);

            var subtotal = receipt.CalculateSubtotal();

            Assert.AreEqual(expectedSubtotal, subtotal);
        }

        [Test]
        public void TestSalesReceiptSingleItemMultipleQtySubtotal()
        {
            // Expected value based on product ID 1 with a Qty of 3
            var expectedSubtotal = 15000000;

            var products = new List<int> { 1, 1, 1};

            var receipt = _receiptService.GenerateReceipt(products);

            var subtotal = receipt.CalculateSubtotal();

            Assert.AreEqual(expectedSubtotal, subtotal);
        }

        [Test]
        public void TestSalesReceiptSingleItemNoDiscount()
        {
            // Expected discount based on product ID 1 with a Qty of 1
            var expectedDiscount = 0;

            var products = new List<int> { 1, 1, 1};

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemFirstDiscount()
        {
            // Expected discount based on product ID 1 with a Qty of 5
            var expectedDiscount = 25000;

            var products = new List<int> { 1, 1, 1, 1, 1 };

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemEmptyDiscount()
        {
            // Expected discount based on product ID 2 with a Qty of 1
            var expectedDiscount = 0;

            var products = new List<int> { 2 };

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemNullDiscount()
        {
            // Expected discount based on product ID 2 with a Qty of 1
            var expectedDiscount = 0;

            var products = new List<int> { 3 };

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemMissingDiscount()
        {
            // Expected discount based on product ID 2 with a Qty of 1
            var expectedDiscount = 0;

            var products = new List<int> { 4 };

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemFirstPlusOneDiscount()
        {
            // Expected discount based on product ID 1 with a Qty of 6
            var expectedDiscount = 30000;

            var products = new List<int> { 1, 1, 1, 1, 1, 1 };

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemThirdDiscount()
        {
            // Expected discount based on product ID 1 with a Qty of 20
            var expectedDiscount = 1000000;

            var products = new List<int>();

            for (int i = 0; i < 20; i++)
            {
                products.Add(1);
            }

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemThirdPlusOneDiscount()
        {
            // Expected discount based on product ID 1 with a Qty of 21
            var expectedDiscount = 1050000;

            var products = new List<int>();

            for (int i = 0; i < 21; i++)
            {
                products.Add(1);
            }

            var receipt = _receiptService.GenerateReceipt(products);

            var discount = receipt.CalculateDiscountTotal();

            Assert.AreEqual(expectedDiscount, discount);
        }

        [Test]
        public void TestSalesReceiptSingleItemSalesTax()
        {
            // Expected sales tax based on product ID 1 with a Qty of 1
            var expectedSalesTax = 526000;

            var products = new List<int> { 1 };

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(expectedSalesTax, receipt.CalculateTax());
        }

        [Test]
        public void TestSalesReceiptSingleItemMultipleSalesTax()
        {
            // Expected sales tax based on product ID 0 with a Qty of 100
            var expectedSalesTax = 36.82m;

            var products = new List<int>();

            for (int i = 0; i < 1000; i++)
            {
                products.Add(0);
            }

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(expectedSalesTax, receipt.CalculateTax());
        }

        [Test]
        public void TestSalesReceiptSingleItemTotalOrder()
        {
            // Expected total based on product ID of 5 with a Qty of 1, sales tax of 10.52 and no discounts
            var expectedTotal = 8841.6;

            var products = new List<int> { 5 };

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(expectedTotal, receipt.CalculateTotalOrderPrice());
        }
        
        [Test]
        public void TestSalesReceiptSingleItemMultipleTotalOrder()
        {
            // Expected total based on product ID of 5 with a Qty of 2, sales tax of 10.52 and line discount of 200
            var expectedTotal = 17462.16;

            var products = new List<int> { 5, 5 };

            var receipt = _receiptService.GenerateReceipt(products);

            Assert.AreEqual(expectedTotal, receipt.CalculateTotalOrderPrice());
        }

        [Test]
        public void TestSalesReceiptNegativeTax()
        {
            var products = new List<Product>
            {
                _dataService.GetProductById(1)
            };

            var order = new Order(products);

            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                // Negative sales tax
                var receipt = new SalesReceipt(order, -24);
            });
        }

    }
}
