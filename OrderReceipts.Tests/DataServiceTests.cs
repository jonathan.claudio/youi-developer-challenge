using NUnit.Framework;
using OrderReceipts.Domain.Interfaces;
using OrderReceipts.Domain.Models;
using OrderReceipts.Domain.ServiceInterfaces;
using OrderReceipts.Domain.Services;

namespace OrderReceipts.Tests
{
    public class DataServiceTests
    {
        private IDataService _dataService;

        [SetUp]
        public void Setup()
        {
            _dataService = new JsonDataService();
        }

        [TearDown]
        public void TearDown()
        {
            _dataService = null;
        }

        [Test]
        public void GetProductsTest()
        {
            var products = _dataService.GetAllProducts();
            Assert.IsNotEmpty(products);
        }

        [Test]
        public void GetSalesTaxTest()
        {
            const decimal expected = 10.52m;

            var salesTax = _dataService.GetSalesTaxPercentage();

            Assert.AreEqual(expected, salesTax);
        }
    }
}