Using the application
- Use Postman to POST an array of integers to the /api/SalesReceipts/GenerateHtmlReceipt or /api/SalesReceipts/GenerateTextReceipt endpoints.  The endpoints should return HTML or Text respectively that have the required information.

Project Structure
- The application code is organised into three projects, one WebAPI, one Domain class library and an NUnit Test class library.
- The OrderReceipts.API project is minimal and only contains logic code to convert data objects into human-friendly formats.
- The OrderReceipts.Domain project contains the application logic.  It also contains the data layer, but this could easily be broken out into its own separate project.
- The OrderReceipts.Tests project contains a series of NUnit test cases to test functionality inside the application.

Implementation Notes
- A mock data service based around JSON files has been implemented.  The implementation is behind an interface so that the 
    mock service can be swapped out for a real service backed by a database or another API.
- I've implemented a bulk item discount as it's probably the most flexible system given the limited amount of time.
- Bulk discounts can also be toggled between a percentage and a flat price discount
- Bulk discounts can be changed easily by changing the "back end" data - the Products.json file in this case.  This would be analogous to changing the discount values in a database.
- Unit tests were written at the same time as the main functionality, as it was critical to testing individual components during development
- Calculation functions could potentially be invoked multiple times during normal use - I'd rather the final number to be correct rather than to make
faulty assumptions.
- The final conversion to a human-friendly format is done at the Controller level, however this could be moved into the Domain if it needed to be reused elsewhere.
- Interfaces are used as much as possible to assist with future maintenance, at the cost of a small short-term time penalty.
- Sales tax can also be changed quickly if possible, if this application were used in a country that is economically unstable. 

Identified Edge Cases:
- Zero or Negative Sales Tax - mitigated on read by not allowing negative sales tax.  >100% sales tax is possible, but unlikely.  0% sales tax is also allowed.
- Missing discounts from the "data service" - if the discount is null and/or missing, it defaults to 0
- Product IDs that do not map to an existing item passed into the API - mitigated by ignoring invalid product IDs
- Items that cost more than the max value of the C# decimal object, of which this application is built around.  This has not been actively mitigated as this is a system limitation and the max value of decimal would be an absurd amount of money.
- Sales tax (GST) might be a flat 10% in Australia, but I can't imagine assuming it's an integer everywhere else in the world, which is why it's represented by a decimal.
- Multiple products in the database could have the same Product ID - this is mitigated somewhat by getting the first item with the product Id.  This would prevent application death/500 errors but could lead to slightly unpredictable or incorrect behaviour. 

Things to do in the future:
- Find slow running sections of code and optimize - possibly the price calculations
- Data Service would be broken out into its own project
- Manipulation of the products in the order could potentially be optimized
- GetLineItems() in SalesReceipt.cs could probably be optimized
- GetProductById() is incredibly inefficient as it reads the file on every call - this approach I believe is more robust (as the file could've changed between executions) but definitely not how I'd implement it if the service was backed by a DB.
- Handle multiples of the same item better - the system is well suited to a variety of items where there are small quantity of each.
    It is easy to pass this data into the API, until there are 10s of the same item (see product ID 0, which is a 50c bolt)
- The LineItem and Product classes could probably be collapsed into a single class, accessed via two different interfaces
- A lot of the SalesReceiptTest logic could be collapsed down into fewer functions, but the bulk of these tests were written prior to a major change in how products are passed into the API.  Tests remain valid despite these changes.
- Fix handling of products in the "database" that have the same Product IDs.
- Unit tests have hardcoded values based on specific product IDs - I would find a better way of handling this.
- Unit tests could be optionally expanded, but I've covered a number of cases and some of the cases cover the entire calculation pipeline.
- HTML handling leaves a lot to be desired - I would use a HTML template given enough time.
- Add integration testing that exercises the API controllerss
- Add automatic build/deploy